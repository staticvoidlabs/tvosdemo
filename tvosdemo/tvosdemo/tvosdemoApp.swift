//
//  tvosdemoApp.swift
//  tvosdemo
//
//  Created by Alexander Handtke on 28.07.23.
//

import SwiftUI

@main
struct tvosdemoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
